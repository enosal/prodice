package P10_2;

import javax.swing.*;

/**
 * Visualizes ButtonFrame1
 * Created by Eryka on 10/29/2015.
 */
public class ButtonView1 {
    public static void main(String[] args) {
        JFrame frame = new ButtonFrame1();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
