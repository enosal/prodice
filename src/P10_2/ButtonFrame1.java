package P10_2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Opens a frame with a button
 * For every click of the button, a message is printed out with the total number of clicks made
 * Created by Eryka on 10/29/2015.
 */
public class ButtonFrame1 extends JFrame {
    private static final int FRAME_WIDTH = 100;
    private static final int FRAME_HEIGHT = 100;
    private int clickCount;

    public ButtonFrame1() {
        clickCount = 0;
        createComponents();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    private void createComponents() {
        JButton button = new JButton("Click me!");
        JPanel panel = new JPanel();
        panel.add(button);
        add(panel);

        ActionListener listener = new ClickListener();
        button.addActionListener(listener);
    }

    public class ClickListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            clickCount += 1;
            System.out.println("I was clicked " + clickCount + " times!");
        }

    }

}
