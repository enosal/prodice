package P10_26;

import javax.swing.*;
import java.awt.*;
import java.awt.Graphics;

/**
 * Component for drawing Olympic rings
 * Created by Eryka on 10/31/2015.
 */
public class OlympicComponent extends JComponent {
    //Setup diameter and line_width values
    private final static int DIAMETER = 100;
    private final static int LINE_WIDTH = 8;

    //Component to paint on
    public void paintComponent(Graphics g) {
        drawRing(g, Color.blue, 0,0); //blue top left
        drawRing(g, Color.black, DIAMETER,0); //black top middle
        drawRing(g, Color.red, 2*DIAMETER, 0); //red top right
        drawRing(g, Color.yellow, (int) (.5*DIAMETER), (int) (.5*DIAMETER) ); //yellow bottom left
        drawRing(g, Color.green, (int) (.5*DIAMETER + DIAMETER), (int) (.5*DIAMETER)); //green bottom right

    }

    //Method to draw ring
    public void drawRing(Graphics g, Color color, int x, int y) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(color);
        g2.setStroke(new BasicStroke(LINE_WIDTH));
        g2.drawOval(x, y, DIAMETER, DIAMETER);
        repaint();
    }
}
