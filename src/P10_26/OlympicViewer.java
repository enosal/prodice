package P10_26;

import javax.swing.*;

/**
 * Program to view Olympic Logo
 * Created by Eryka on 10/31/2015.
 */
public class OlympicViewer {
    public static void main(String[] args) {
        //Set frame's height and weight
        final int FRAME_WIDTH = 325;
        final int FRAME_HEIGHT = 200;

        //Create frame & settings
        JFrame olympicFrame = new JFrame();
        olympicFrame.setTitle("Olympic Logo");
        olympicFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        olympicFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and add component to frame for drawing
        JComponent olympicComp = new OlympicComponent();
        olympicFrame.add(olympicComp);

        //Set frame visible
        olympicFrame.setVisible(true);
    }
}
