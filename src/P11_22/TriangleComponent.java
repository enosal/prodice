package P11_22;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Line2D;
import java.util.ArrayList;

/**
 * Draws a triangle with 3 mouse clicks
 * Click once to set the first point
 * Click a second time to set the second point and draw the first line
 * Click a third time to set the third point and connect the third point to the first and second points to create the final two lines
 * Click a fourth time to reset and clean the previously drawn triangle
 * Created by Eryka on 10/31/2015.
 */
public class TriangleComponent extends JComponent {
    //Declare variables
    private Point point1;
    private Point point2;
    private Point point3;
    private int clickCount;
    private int x;
    private int y;

    /*
        Construct TriangleComponent
     */
    public TriangleComponent() {
        //initialize defaults
        reset();

        //Add mouseclick listener
        MouseListener listener = new MouseClickListener();
        addMouseListener(listener);
    }

    /*
        Inner Class to listen to mouse clicks
     */
    class MouseClickListener extends MouseAdapter {
        /*
            When the mouse is pressed, get the x coordinate, y coordinate, increment the click counter, repaint (call paintComponent)
         */
        public void mousePressed(MouseEvent event) {
            x = event.getX();
            y = event.getY();
            clickCount += 1;
            repaint();
        }
    }

    /*
        Resets click count
     */
    public void reset() {
        //reset
        clickCount = 0;
    }

    /*
        Component to paint on
     */
    public void paintComponent(Graphics g) {
        if (clickCount == 1) {
            point1 = new Point(x,y);
            makeLine(g, point1, point1);
        }
        else if (clickCount == 2) {
            point2 = new Point(x,y);
            makeLine(g, point1, point2);

        }
        else if (clickCount == 3) {
            point3 = new Point(x,y);
            makeLine(g, point1, point2);
            makeLine(g, point1, point3);
            makeLine(g, point2, point3);

        }
        else if (clickCount == 4) {
            reset();
        }

    }

    /*
        Draws the line from p1 to p2
     */
    public void makeLine(Graphics g, Point p1, Point p2) {
        g.drawLine(p1.x, p1.y, p2.x, p2.y);
    }

}
