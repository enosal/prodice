package P11_22;

import javax.swing.*;

/**
 * Views TriangleComponent
 * Created by Eryka on 10/31/2015.
 */
public class TriangleViewer {
    public static void main(String[] args) {
        final int FRAME_WIDTH = 500;
        final int FRAME_HEIGHT = 500;

        //Create frame & settings
        JFrame triangleFrame = new JFrame();
        triangleFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        triangleFrame.setTitle("Click three times to create a triangle!");
        triangleFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and add component to frame for drawing
        JComponent triangleComp = new TriangleComponent();
        triangleFrame.add(triangleComp);

        //Set frame visible
        triangleFrame.setVisible(true);
    }
}
