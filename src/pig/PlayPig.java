package pig;

import javax.swing.*;

/**
 * Program to start playing Pig
 * Created by Eryka on 11/1/2015.
 */
public class PlayPig {
    public static void main(String[] args) {
        //Create frame & settings
        JFrame pigFrame = new PigFrame();
        pigFrame.setTitle("Pig");
        pigFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pigFrame.setVisible(true);
    }
}
