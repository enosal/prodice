package pig;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

/**
 * User rolls a dice as many times as desired either until user rolls a 1 or user choose to hold
 * If user chooses to hold, they get all the points from previous die rolls
 * If user rolls a 1 prior to holding, they lose all points from previous die rolls
 * Computer takes a turn.
 * First player to 100 points wins.
 * Created by Eryka on 11/1/2015.
 */
public class PigFrame extends JFrame {
    //Set frame's height and weight
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 400;

    //For Display
    private JPanel userPanel;
    private JPanel compPanel;
    private JLabel userPoints;
    private JLabel compPoints;
    private JLabel userStatus;
    private JLabel compStatus;
    private JLabel userResult;
    private JLabel compResult;
    private JButton buttonRoll;
    private JButton buttonHold;
    private JButton buttonStart;

    //For roll dices
    private ArrayList<Integer> userRolls;
    private ArrayList<Integer> compRolls;
    private int userTot;
    private int compTot;

    //For game play
    private final int WINNING_POINTS = 100;
    private String currentTurn;
    private int currentRoll;
    private int addedScore;
    Random compThrowsGenerator;
    int compCurrentThrow;
    int compNumOfThrows;
    boolean computerHold;
    boolean userThrew;

    //for Timer
    final int INTERVAL = 1200;
    TimerListener compTimerListener;
    Timer t;

    /*
        Create Frame
     */
    public PigFrame() {
        //Set frame width/height
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setLayout(new GridLayout(2,1));

        //Create Panels
        createPanels();

        //Reset everything
        resetGame();

    }

    /*
        Resets Game.
     */
    public void resetGame() {

        //Initialize for roll dices
        userRolls = new ArrayList<>();
        compRolls = new ArrayList<>();
        userTot = 0;
        compTot = 0;

        //Initialize for gamepaly
        currentTurn = "User";
        currentRoll = 0;        //the roll for the turn
        addedScore = 0;         //score to be added for a turn
        compThrowsGenerator = new Random(); //random number generator for the computer's turn
        computerHold = true;    //whether the computer "chose" to hold
        userThrew = false;      //whether the user made at least one role dice in their turn
        compCurrentThrow = 1;   //how many times the computer has thrown the dice in that turn

        //Initialize for timer
        compTimerListener = new TimerListener();
        t = new Timer(INTERVAL, compTimerListener);

        //Initialize label texts
        userPoints.setText("Your Points: 0");
        userStatus.setText("Roll to begin!");
        userResult.setText("");
        compPoints.setText("Computer's Points: 0");
        compStatus.setText("Waiting for User");
        compResult.setText("");

        //Enable buttons
        buttonRoll.setEnabled(true);
        buttonHold.setEnabled(true);

    }

    /*
        Lays out user and computer panels
     */
    public void createPanels() {
        userPanel = new JPanel();
        compPanel = new JPanel();

        userPanel = createUserPanel(userPanel);
        compPanel = createCompPanel(compPanel);

        add(userPanel);
        add(compPanel);
    }

    /*
        Creates user's panel
     */
    public JPanel createUserPanel(JPanel uPanel) {
        //Set UserPanel's settings
        uPanel.setBorder(new TitledBorder(new LineBorder(Color.MAGENTA), "YOU"));
        uPanel.setLayout(new GridLayout(4,1));

        //Create Point label, put it FIRST
        userPoints = new JLabel();
        uPanel.add(userPoints);

        //Create Buttons
        buttonRoll = new JButton("Roll");
        buttonHold = new JButton("Hold");
        buttonStart = new JButton("Start Over");
        ActionListener rollListener = new AddRollListener();
        ActionListener holdListener = new AddHoldListener();
        ActionListener startListener = new AddStartListener();
        buttonRoll.addActionListener(rollListener);
        buttonHold.addActionListener(holdListener);
        buttonStart.addActionListener(startListener);

        //Create Button Panel, put it SECOND
        JPanel userSecondPanel = new JPanel();
        userSecondPanel.setLayout(new GridLayout(1,3));
        userSecondPanel.add(buttonRoll);
        userSecondPanel.add(buttonHold);
        userSecondPanel.add(buttonStart);
        uPanel.add(userSecondPanel);

        //Put Status and Result Panels THIRD and FOURTH
        userStatus = new JLabel();
        userStatus.setHorizontalAlignment(JLabel.CENTER);
        uPanel.add(userStatus);
        userResult = new JLabel();
        userResult.setHorizontalAlignment(JLabel.CENTER);
        uPanel.add(userResult);

        return uPanel;
    }

    /*
        Creates computer's panel
     */
    public JPanel createCompPanel(JPanel cPanel) {
        //settings
        cPanel.setBorder(new TitledBorder(new LineBorder(Color.PINK), "COMPUTER"));
        cPanel.setLayout(new GridLayout(3,1));

        //Create PC's point label
        compPoints = new JLabel();

        //Create PC's Status and Result label
        compStatus = new JLabel();
        compStatus.setHorizontalAlignment(JLabel.CENTER);
        compResult = new JLabel();
        compResult.setHorizontalAlignment(JLabel.CENTER);

        //Add to PC Panel
        cPanel.add(compPoints);
        cPanel.add(compStatus);
        cPanel.add(compResult);


        return cPanel;
    }

    /*
        Listens for "Roll" button clicks
     */
    class AddRollListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            userThrew = true;
            if (currentTurn.equals("User")) {
                currentRoll = roll(userRolls);
                if (currentRoll == 1) {
                    userStatus.setText("You rolled: " + userRolls.toString());
                    userResult.setText("No points added. Wait for your next turn");
                    endOfTurn(currentTurn, userTot);
                }
                else {
                    userStatus.setText("You rolled: " + currentRoll + ". Roll again or Hold?");
                    userResult.setText(userRolls.toString());
                }
            }
        }
    }

    /*
        Listens for "Hold" button clicks
     */
    class AddHoldListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            //if User did not make a previous roll
            if (!userThrew) {
                userStatus.setText("Please make at least one roll prior to holding!");
            }
            else {
                if (currentTurn.equals("User")) {
                    addedScore = hold(userRolls);
                    userTot += addedScore;
                    userPoints.setText("Your Points: " + userTot);
                    userStatus.setText("You rolled: " + userRolls.toString() + " and Held");
                    userResult.setText(addedScore + " points added to your score! Wait for your next turn");
                    endOfTurn(currentTurn, userTot);

                }
            }
        }
    }

    /*
        Listens for "Start Over" button clicks
     */
    class AddStartListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            resetGame();
        }
    }

    /*
        Rolls one die, adds die role to the player's list of current rolls
     */
    public int roll(ArrayList<Integer> anyRolls) {
        Random generator = new Random();
        int rolledNum = generator.nextInt(6) + 1;
        anyRolls.add(rolledNum);
        return rolledNum;
    }

    /*
        Adds all current roll dies to player's score
     */
    public int hold(ArrayList<Integer> anyRolls) {
        int sum = 0;
        for (int anyRoll : anyRolls) {
            sum += anyRoll;
        }
        return sum;
    }

    /*
        Implements the computer's turn
     */
    public void computersTurn() {
        int INTERVAL = 1200; //ms
        computerHold = true;
        t.start();
    }

    /*
        Timer to simulate the computer's rolls
     */
    class TimerListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            //if the computer's current throw is less than the the number of rolls the computer has "chosen" for that turn, the computer keeps rolling
            if (compCurrentThrow <= compNumOfThrows) {
                currentRoll = roll(compRolls);
                if (currentRoll == 1) {
                    computerHold = false;
                    compCurrentThrow = compNumOfThrows + 1;
                }
                else {
                    compStatus.setText("Computer rolled: " + currentRoll);
                    compResult.setText(compRolls.toString());
                    compCurrentThrow++;
                }
            }
            else {
                //if the computer did Hold
                if (computerHold) {
                    addedScore = hold(compRolls);
                    compTot += addedScore;
                    compPoints.setText("Computer's Points: " + compTot);
                    compStatus.setText("Computer rolled: " + compRolls.toString() + " and Held.");
                    compResult.setText(addedScore + " points added to the Computer's score! Waiting for User.");
                }
                //if the computer did NOT hold and rolled a 1
                else {
                    compStatus.setText("Computer rolled: " + compRolls.toString());
                    compResult.setText("No points added. Waiting for User.");
                }
                endOfTurn(currentTurn, compTot);
                t.stop();
            }

        }
    }

    /*
        Simulates the end of the player's turn, resets the turn
     */
    public void endOfTurn(String player, int tot) {
        //check the player's score
        if (tot >= WINNING_POINTS) {
            if (player.equals("User")) {
                userStatus.setText("");
                userResult.setText("YOU WIN!");
                compStatus.setText("");
                compResult.setText("YOU LOSE!");
            }
            else if (player.equals("Comp")) {
                userStatus.setText("");
                userResult.setText("YOU LOSE!");
                compStatus.setText("");
                compResult.setText("YOU WIN!");
            }
            buttonRoll.setEnabled(false);
            buttonHold.setEnabled(false);
        }
        else {
            userRolls.clear();
            compRolls.clear();
            userThrew = false;
            compCurrentThrow = 1;
            if (player.equals("User")) {
                buttonRoll.setEnabled(false);
                buttonHold.setEnabled(false);
                currentTurn = "Comp";
                compNumOfThrows = compThrowsGenerator.nextInt(5) + 2;
                compStatus.setText("COMPUTER'S TURN");
                compResult.setText("");
                computersTurn();
            }
            else if (player.equals("Comp")) {
                buttonRoll.setEnabled(true);
                buttonHold.setEnabled(true);
                currentTurn = "User";
                userStatus.setText("YOUR TURN");
                userResult.setText("");
            }
        }


    }
}