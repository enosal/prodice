package P10_35;

import P10_26.OlympicComponent;

import javax.swing.*;

/**
 * Program to view BillFrame
 * Created by Eryka on 10/31/2015.
 */
public class BillViewer {
    public static void main(String[] args) {
        //Create frame & settings
        JFrame rstFrame = new BillFrame();
        rstFrame.setTitle("Yum Foods Restaurant");
        rstFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        rstFrame.setVisible(true);
    }
}
