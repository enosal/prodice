package P10_35;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Shows a Bill
 * Created by Eryka on 10/31/2015.
 */
public class BillFrame extends JFrame {
    //Set frame's height and weight
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 400;

    //Declare certain components accessible to all methods
    private JTextArea textArea;
    private JTextField nameField;
    private JTextField priceField;
    private double total;
    private double tax;
    private double tip;
    private JLabel totalLabel;
    private JLabel taxLabel;
    private JLabel finTotalLabel;
    private JLabel tipLabel;


    public BillFrame() {
        //Set frame width/height
        setSize(FRAME_WIDTH, FRAME_HEIGHT);

        //Create TextArea
        textArea = createTextArea();

        //Create Panels
        createPanels();
    }

    /*
        Create panels
    */
    public void createPanels() {

        //Panels
        JPanel buttonPanel = createButtonPanel();
        JPanel textFieldPanel = createTextFieldPanel();
        JPanel textAreaPanel = createTextAreaPanel();
        JPanel totalPanel = createTotalPanel();

        //Right panel (Text/text area/total)
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new GridLayout(3,1));
        rightPanel.add(textFieldPanel);
        rightPanel.add(textAreaPanel);
        rightPanel.add(totalPanel);

        //main panel & layout
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(1,2));
        mainPanel.add(buttonPanel);
        mainPanel.add(rightPanel);
        add(mainPanel);
    }

    /*
        Create and return text area for displaying items
     */
    public JTextArea createTextArea() {
        //TextArea field
        final int ROWS = 5;
        final int COLS = 13;
        textArea = new JTextArea(ROWS, COLS);
        textArea.setEditable(false);
        return textArea;
    }

    /*
        Create and return Button Panel for popular menu items
     */
    public JPanel createButtonPanel() {
        //buttons
        JButton button1 = new JButton("Chocolate Shake - $2.00");
        JButton button2 = new JButton("Mai Tai - $10.00");
        JButton button3 = new JButton("Zucchini Fries - $4.50");
        JButton button4 = new JButton("Onion Rings - $4.50");
        JButton button5 = new JButton("Fried Rice - $4.50");
        JButton button6 = new JButton("Steak - $15.00");
        JButton button7 = new JButton("Chicken - $15.00");
        JButton button8 = new JButton("Fish - $15.00");
        JButton button9 = new JButton("Cheesecake - $7.00");
        JButton button10 = new JButton("Brownie Sundae - $7.00");

        //Add Listeners
        ActionListener listener = new AddItemButtonListener();
        button1.addActionListener(listener);
        button2.addActionListener(listener);
        button3.addActionListener(listener);
        button4.addActionListener(listener);
        button5.addActionListener(listener);
        button6.addActionListener(listener);
        button7.addActionListener(listener);
        button8.addActionListener(listener);
        button9.addActionListener(listener);
        button10.addActionListener(listener);


        //Left panel (buttons)
        JPanel buttonPanel = new JPanel();
        buttonPanel.setBorder(new TitledBorder(new EtchedBorder(), "Our Popular Items"));
        buttonPanel.setLayout(new GridLayout(10,1));
        buttonPanel.add(button1);
        buttonPanel.add(button2);
        buttonPanel.add(button3);
        buttonPanel.add(button4);
        buttonPanel.add(button5);
        buttonPanel.add(button6);
        buttonPanel.add(button7);
        buttonPanel.add(button8);
        buttonPanel.add(button9);
        buttonPanel.add(button10);
        return buttonPanel;
    }

    /*
        Create and return Text Field Panel for custom item entry
     */
    public JPanel createTextFieldPanel() {
        //TextField
        final int NAME_WIDTH = 15;
        final int PRICE_WIDTH = 5;
        JLabel name = new JLabel("Name of item:");
        nameField = new JTextField(NAME_WIDTH);
        JLabel price = new JLabel("Price of item: $");
        priceField = new JTextField(PRICE_WIDTH);
        JButton addCustomItem = new JButton("Add Item");

        //Add Listener
        ActionListener listener = new AddItemCustomListener();
        addCustomItem.addActionListener(listener);

        //Textfield panel
        JPanel textFieldPanel = new JPanel();
        textFieldPanel.setBorder(new TitledBorder(new EtchedBorder(), "Add your own item"));
        textFieldPanel.setLayout(new GridLayout(3,2));
        textFieldPanel.add(name);
        textFieldPanel.add(nameField);
        textFieldPanel.add(price);
        textFieldPanel.add(priceField);
        textFieldPanel.add(new JLabel());
        textFieldPanel.add(addCustomItem);
        return textFieldPanel;
    }

    /*
        Create and return Text Area Panel for displaying items
     */
    public JPanel createTextAreaPanel() {
        //Textarea panel
        JPanel textAreaPanel = new JPanel();
        textAreaPanel.setLayout(new GridLayout(1,1));
        textAreaPanel.add(textArea);
        JScrollPane scrollPane = new JScrollPane(textArea);
        textAreaPanel.add(scrollPane);
        textAreaPanel.setBorder(new TitledBorder(new EtchedBorder(), "Ordered Items"));
        return textAreaPanel;

    }

    /*
        Create and return Panel that holds all the total/tax/tip info
     */

    public JPanel createTotalPanel(){
        total = 0.00;
        tax = 0.00;
        tip = 0.00;
        totalLabel = new JLabel("$0.00");
        taxLabel = new JLabel("$0.00");
        finTotalLabel = new JLabel("$0.00");
        tipLabel = new JLabel("$0.00");

        JPanel totalPanel = new JPanel();
        totalPanel.setBorder(new TitledBorder(new EtchedBorder(), "Receipt"));
        totalPanel.setLayout(new GridLayout(5,2));
        totalPanel.add(new JLabel("Total:"));
        totalPanel.add(totalLabel);
        totalPanel.add(new JLabel("Tax:"));
        totalPanel.add(taxLabel);
        totalPanel.add(new JLabel("FINAL TOTAL:"));
        totalPanel.add(finTotalLabel);
        totalPanel.add(new JLabel());
        totalPanel.add(new JLabel());
        totalPanel.add(new JLabel("Tip (15%):"));
        totalPanel.add(tipLabel);
        return totalPanel;
    }

    /*
        Creates listeners for the Button Panel
     */

    class AddItemButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            String itemLine = event.getActionCommand();
            textArea.append(itemLine + "\n");
            String[] lineSplit1 = itemLine.split("-");
            System.out.println(lineSplit1[1]);
            String[] lineSplit2 = (lineSplit1[1]).split("[$]");

            try {
                double price = Double.parseDouble(lineSplit2[1]);

                total += price;
                totalLabel.setText(String.format("$%.2f", total));

                tax = total * 0.1;
                taxLabel.setText(String.format("$%.2f", tax));

                finTotalLabel.setText(String.format("$%.2f", (total + tax)));

                tip = .15 * total;
                tipLabel.setText(String.format("$%.2f", tip));
            }
            catch (NumberFormatException exception) {
                System.out.println("Item not added");
            }
        }
    }

    /*
        Creates listeners for adding a custom item
     */
    class AddItemCustomListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            try {
                String name = nameField.getText();
                double price = Double.parseDouble(priceField.getText());
                textArea.append(String.format("%s - $%.2f%n", name, price));
                total += price;
                totalLabel.setText(String.format("$%.2f", total));

                tax = total * 0.1;
                taxLabel.setText(String.format("$%.2f", tax));

                finTotalLabel.setText(String.format("$%.2f", (total + tax)));

                tip = .15 * total;
                tipLabel.setText(String.format("$%.2f", tip));


            }
            catch (NumberFormatException exception) {
                System.out.println("Item not added");
            }



        }

    }

}
